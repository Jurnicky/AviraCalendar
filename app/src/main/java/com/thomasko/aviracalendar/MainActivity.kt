package com.thomasko.aviracalendar

import android.os.Bundle
import com.thomasko.aviracalendar.base.injection.injector
import com.thomasko.aviracalendar.base.ui.lifecycle.BaseActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injector.inject(this)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
    }

    override val mainContentResourceId: Int
        get() = R.id.main_content
}
