package com.thomasko.aviracalendar.base.ui.list

import android.support.v7.widget.RecyclerView
import android.view.View

abstract class BaseHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
