package com.thomasko.aviracalendar.base.injection

import com.thomasko.aviracalendar.AviraCalendarApplication
import com.thomasko.aviracalendar.MainActivity
import com.thomasko.aviracalendar.addbirthday.ui.AddBirthdayFragment
import com.thomasko.aviracalendar.base.ui.lifecycle.BaseActivity
import com.thomasko.aviracalendar.homepage.injection.DataModule
import com.thomasko.aviracalendar.homepage.ui.HomePageFragment
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(
        ApplicationModule::class,
        DataModule::class
))
@Singleton
internal interface ApplicationComponent {
    fun inject(application: AviraCalendarApplication)

    fun inject(activity: MainActivity)

    fun inject(activity: BaseActivity)

    fun inject(fragment: HomePageFragment)

    fun inject(fragment: AddBirthdayFragment)
}