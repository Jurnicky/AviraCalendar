package com.thomasko.aviracalendar.base.ui.lifecycle

import android.os.Bundle
import com.thomasko.aviracalendar.base.infrastructure.Logger
import com.thomasko.aviracalendar.base.infrastructure.i
import javax.inject.Inject

abstract class MvpFragment<
        P : MvpPresenter<V>,
        V : MvpView> : BaseFragment(), Logger {

    internal lateinit var presenter: P

    abstract fun getMvpView(): V

    override fun onStart() {
        super.onStart()
        presenter.bindView(getMvpView())
        presenter.onAfterStart()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.onAfterCreate()
    }

    @Inject
    fun setPresenter(presenter: P) {
        this.presenter = presenter
    }

    override fun onDestroyView() {
        super.onDestroyView()

        presenter.unbindView()

        if (activity.isFinishing) {
            i { "===== activityIsFinishing" }
            presenter.destroyView()
        }

        if (isRemoving) {
            i { "===== isRemoving, saveInstanceCalled " }
            presenter.destroyView()
        }
    }
}
