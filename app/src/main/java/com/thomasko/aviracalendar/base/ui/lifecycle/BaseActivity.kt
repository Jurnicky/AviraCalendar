package com.thomasko.aviracalendar.base.ui.lifecycle

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.annotation.IdRes
import com.thomasko.aviracalendar.base.application.AndroidController
import com.thomasko.aviracalendar.base.injection.injector
import com.thomasko.aviracalendar.homepage.ui.HomePageFragment
import com.thomasko.aviracalendar.utils.replaceFragment
import javax.inject.Inject

@SuppressLint("Registered")
open class BaseActivity : LogActivity() {

    @Inject lateinit var androidController: AndroidController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injector.inject(this)
        androidController.baseActivity = this
        showHomePage()
    }

    @get:IdRes
    override val mainContentResourceId: Int
        get() = throw RuntimeException("Extending activity must override this method")

    private fun showHomePage() {
        replaceFragment(HomePageFragment.create())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        androidController.onActivityResult(requestCode, resultCode, data)
    }
}
