package com.thomasko.aviracalendar.base.ui.list

import com.thomasko.aviracalendar.base.ui.lifecycle.MvpPresenter

abstract class ListPresenter<V : ListView<E>, E> : MvpPresenter<V>() {

    protected abstract var items: List<E>

    internal var isLoading = false

    override fun updateAllFieldsOnView() {
        super.updateAllFieldsOnView()
        val items = items
        view?.setItems(items)
    }

    internal open fun onLastPositionScrolled() {}

    protected fun notifyItemsLoaded() {
        isLoading = false
    }

    protected fun notifyItemsLoading() {
        isLoading = true
    }
}
