package com.thomasko.aviracalendar.homepage.ui

import com.thomasko.aviracalendar.base.ui.list.ListView
import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail

interface HomePageView : ListView<BirthdayDetail>