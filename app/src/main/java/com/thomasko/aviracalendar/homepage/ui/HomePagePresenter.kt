package com.thomasko.aviracalendar.homepage.ui

import com.thomasko.aviracalendar.addbirthday.application.BirthdayController
import com.thomasko.aviracalendar.base.ui.list.ListPresenter
import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail
import javax.inject.Inject

internal class HomePagePresenter @Inject constructor() : ListPresenter<HomePageView, BirthdayDetail>() {

    @Inject lateinit var birthdayController: BirthdayController

    override var items: List<BirthdayDetail> = listOf()

    internal fun readBirthday() = birthdayController.getBirthdays().subscribe { list ->
        items = list
        view?.notifyDataSetChanged()
    }
}
