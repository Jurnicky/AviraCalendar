package com.thomasko.aviracalendar.speech.application

import android.content.ActivityNotFoundException
import com.thomasko.aviracalendar.R
import com.thomasko.aviracalendar.base.application.AndroidController
import com.thomasko.aviracalendar.base.application.REQ_CODE_SPEECH_INPUT
import com.thomasko.aviracalendar.utils.showToast
import com.thomasko.aviracalendar.utils.speechRecognization
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SpeechRecognizationController @Inject constructor() {

    @Inject lateinit var androidController: AndroidController

    internal fun speachIntent() = androidController.apply {
        baseActivity.apply {
            try {
                speechRecognization(getString(R.string.speech_recognize), REQ_CODE_SPEECH_INPUT)
            } catch (e: ActivityNotFoundException) {
                baseContext.showToast(getString(R.string.speech_error))
            }
        }
    }
}