package com.thomasko.aviracalendar.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.provider.CalendarContract
import android.speech.RecognizerIntent
import android.support.v4.app.ActivityCompat.startActivityForResult
import android.widget.Toast
import org.joda.time.LocalDate
import java.util.*

internal fun Context.calendarIntentAddEvent(beginTime: LocalDate, title: String) = startActivity(Intent(Intent.ACTION_INSERT).apply {
    data = CalendarContract.Events.CONTENT_URI
    putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.toDate().time)
    putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true)
    putExtra(CalendarContract.Events.TITLE, title)
})

internal fun Activity.speechRecognization(text: String, requestCode: Int) = startActivityForResult(Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
    putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
    putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
    putExtra(RecognizerIntent.EXTRA_PROMPT, text)
}, requestCode)

internal fun Context.showToast(text: String) = Toast.makeText(this, text, Toast.LENGTH_SHORT).show()