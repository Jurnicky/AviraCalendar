package com.thomasko.aviracalendar.utils

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.thomasko.aviracalendar.base.ui.lifecycle.BaseActivity
import com.thomasko.aviracalendar.base.ui.lifecycle.BaseFragment

@SuppressLint("CommitTransaction")
internal fun BaseActivity.replaceFragment(fragment: BaseFragment, addToBackStack: Boolean = false, shared: Map<String, View>? = null) {
    val transition = supportFragmentManager.beginTransaction().replace(mainContentResourceId, fragment, fragment.managerTag())
    addToBackStack.then { transition.addToBackStack(null) }
    shared?.forEach { transition.addSharedElement(it.value, it.key) }
    transition.commit()
}

internal fun <T : BaseFragment> T.withArgs(map: Map<String, Any>): T {
    val args = Bundle(map.size)
    map.forEach { args.putArguments(it.key, it.value) }
    arguments = args
    return this
}

internal fun Bundle.putArguments(key: String, value: Any) = when (value) {
    is String -> putString(key, value)
    is Int -> putInt(key, value)
    is Long -> putLong(key, value)
    is Boolean -> putBoolean(key, value)
    else -> throw UnsupportedOperationException("Not implemented for " + value.javaClass.simpleName)
}
