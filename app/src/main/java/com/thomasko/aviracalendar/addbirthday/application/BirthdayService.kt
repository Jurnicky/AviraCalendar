package com.thomasko.aviracalendar.addbirthday.application

import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail
import io.reactivex.Single

interface BirthdayService {

    fun getBirthdayData(): Single<List<BirthdayDetail>>

    fun addBirthdayData(item: BirthdayDetail)
}