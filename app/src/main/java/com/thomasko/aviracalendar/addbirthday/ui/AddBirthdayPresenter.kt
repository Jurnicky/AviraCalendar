package com.thomasko.aviracalendar.addbirthday.ui

import android.app.DatePickerDialog
import android.content.Intent
import android.speech.RecognizerIntent
import com.thomasko.aviracalendar.addbirthday.application.BirthdayController
import com.thomasko.aviracalendar.base.application.ActivityResultListener
import com.thomasko.aviracalendar.base.application.AndroidController
import com.thomasko.aviracalendar.base.application.REQ_CODE_SPEECH_INPUT
import com.thomasko.aviracalendar.base.ui.lifecycle.MvpPresenter
import com.thomasko.aviracalendar.datepicker.application.DatepickerController
import com.thomasko.aviracalendar.homepage.domain.BirthdayDetail
import com.thomasko.aviracalendar.speech.application.SpeechRecognizationController
import com.thomasko.aviracalendar.utils.then
import org.joda.time.LocalDate
import javax.inject.Inject


class AddBirthdayPresenter @Inject constructor() : MvpPresenter<AddBirthdayView>(), ActivityResultListener {

    @Inject lateinit var datePickerController: DatepickerController
    @Inject lateinit var speechRecognizationController: SpeechRecognizationController
    @Inject lateinit var androidController: AndroidController
    @Inject lateinit var birtdayController: BirthdayController

    internal var titleEvent: String? = null
    private val dateTimeNow = LocalDate.now()
    private var selectedDate = dateTimeNow

    override fun updateAllFieldsOnView() {
        super.updateAllFieldsOnView()
        view?.setDate(dateTimeNow.toString())
        androidController.activityResultListener = this
    }

    fun couldAddToCalendar() {
        if (titleEvent.isNullOrBlank())
            view?.showValidationError()
        else {
            addNewBirthday()
            view?.openCalendar(selectedDate, titleEvent!!)
        }
    }

    private fun addNewBirthday() = birtdayController.addBirthday(BirthdayDetail().apply {
        name = titleEvent
        date = selectedDate.toDate().time
    })


    fun showDatePicker() = datePickerController.showDatepickerWithActualDate(
            DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                selectedDate = LocalDate(year, month + 1, day)
                view?.setDate(selectedDate.toString())
            })

    fun speechRecognize() = speechRecognizationController.speachIntent()

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        (requestCode == REQ_CODE_SPEECH_INPUT).then {
            data?.let {
                val result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                view?.setName(result.first())
            }
        }
    }
}